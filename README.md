# Paragraphs Sets UI

# Instalation
Follow default composer modules installation flow, by running command:
```composer require 'drupal/paragraphs_sets_plugins_ui:^1.0'```

# Requirements
Requires [Paragraphs Sets](https://www.drupal.org/project/paragraphs_sets) module 

# Usage
1. Add field of type "paragraph" to your content type.
2. Go to crete/edit page of the content type node.
3. Add paragraphs to the field from step 1.
4. Fil lin title field for the new paragrap set.
5. Click "Save as a new set (give it a title)" -> the paragraph set with the paragraphs structure (no content) based on paragraphs added in step 3 will be created.
6. Click "Save set (With content)" -> the paragraph set with the paragraphs structure and content (texts, images, links, etc.) based on paragraphs added in step 3 will be created.
7. You can find and manage your sets at the rout entity.paragraphs_set.collection (/admin/structure/paragraphs_set) provided by the parent paragraphs_sets module.
