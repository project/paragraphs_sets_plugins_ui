<?php

namespace Drupal\paragraphs_sets_plugins_ui;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_sets\Entity\ParagraphsSet;

/**
 * Class ParagraphsSetsCreator.
 */
class ParagraphsSetsCreator {

  /**
   * Allowed field types for the structure with content.
   *
   * You need to test if a field type works with this functionality
   * (saving set with the content)
   * before adding it here.
   */
  const ALLOWED_TYPES = [
    'integer',
    'boolean',
    'entity_reference',
    'string',
    'string_long',
    'list_string',
    'block_field',
    'text',
    'text_long',
    'image',
    'color_field_type',
    'viewsreference',
    'datetime',
    'float',
    'list_integer',
    'webform',
    'field_wistia',
    'text_with_summary',
    'tablefield',
    'range_integer',
    'link',
  ];

  /**
   * Drupal\Component\Datetime\TimeInterface definition.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $datetimeTime;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Component\Transliteration\TransliterationInterface definition.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * Constructs a new ParagraphsSetsCreate object.
   */
  public function __construct(TimeInterface $datetime_time, DateFormatterInterface $date_formatter, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, TransliterationInterface $transliteration) {
    $this->datetimeTime = $datetime_time;
    $this->dateFormatter = $date_formatter;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->transliteration = $transliteration;
  }

  /**
   * Generates paragraphs set.
   *
   * @param array $structure
   *   Paragraphs structure.
   * @param string $title
   *   Preset title.
   *
   * @return \Drupal\paragraphs_sets\Entity\ParagraphsSet|null
   *   Paragraphs set.
   */
  function generatePreset(array $structure, $title = '') {
    if (empty($title)) {
      $title = 'Generated: ' . $this->dateFormatter->format($this->datetimeTime->getRequestTime());
    }
    $id = $this->getMachineName($title);
    $config_name = 'paragraphs_sets.set.' . $id;
    $config = $this->configFactory->getEditable($config_name);
    $config->set('id', $id);
    $config->set('label', $title);
    $config->set('paragraphs', $structure);
    $config->save();

    return $this->entityTypeManager
      ->getStorage('paragraphs_set')
      ->load($id);
  }

  /**
   * Helper structure extractor, top level.
   *
   * @param $paragraphs
   *   Paragraphs data array.
   * @param bool $structure_only
   *   Flag to save structure without content.
   *
   * @return array
   *   Structure array.
   */
  function prepareStructureTop($paragraphs, $structure_only) {
    $structure = [];
    foreach ($paragraphs as $key => $paragraph) {
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      $paragraph = $paragraph['entity'];
      $structure[$key]['bundle'] = $paragraph->bundle();

      // Prepare sub paragraphs structure.
      if ($data = $this->prepareStructureSub($paragraph, $structure_only)) {
        $structure[$key]['data'] = $data;
      }

      // Prepare fields content for the top level.
      if (!$structure_only) {
        $this->prepareContent($paragraph, $structure[$key]);
      }
    }
    return $structure;
  }

  /**
   * Helper structure extractor, sub leveles.
   *
   * Helper function to extract paragraphs data in format
   * ready to be used in Paragraphs set.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragraph to loop.
   * @param bool $structure_only
   *   Flag to save structure without content.
   *
   * @return array
   *   Paragraphs structure array.
   */
  function prepareStructureSub(ParagraphInterface $paragraph, $structure_only) {
    $data = [];
    $fields = $this->entityFieldManager->getFieldDefinitions(
      $paragraph->getEntityTypeId(), $paragraph->bundle()
    );

    foreach ($fields as $field) {
      if ($field->getType() !== 'entity_reference_revisions') {
        continue;
      }

      $field_name = $field->getName();
      /** @var \Drupal\paragraphs\ParagraphInterface[] $sub_paragraphs */
      if ($sub_paragraphs = $paragraph->get($field_name)->referencedEntities()) {
        $data[$field_name]['plugin'] = 'nested_entities';

        foreach ($sub_paragraphs as $key => $sub_paragraph) {
          $data[$field_name]['data'][$key]['bundle'] = $sub_paragraph->bundle();

          if ($sub_data = $this->prepareStructureSub($sub_paragraph, $structure_only)) {
            $sub_fields = array_keys($sub_data);

            foreach ($sub_fields as $sub_field) {
              $data[$field_name]['data'][$key][$sub_field] = $sub_data[$sub_field];
            }
          }

          // Prepare fields content for the sub-levels.
          if (!$structure_only) {
            $this->prepareContent(
              $sub_paragraph, $data[$field_name]['data'][$key], FALSE
            );
          }
        }
      }
    }

    return $data;
  }

  /**
   * Prepares paragraph content for the set.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragrpahs entity.
   *
   * @param $data
   *   Fields data.
   * @param bool $top
   *   TRUE if this is top level
   *   as top and sub-levels have a bit of different data structure.
   */
  function prepareContent(ParagraphInterface $paragraph, &$data, $top = TRUE) {
    $fields = $this->entityFieldManager->getFieldDefinitions(
      $paragraph->getEntityTypeId(), $paragraph->bundle()
    );

    foreach ($fields as $field) {
      $field_name = $field->getName();
      // Do not save properties and empty fields,
      // also only process fields types that work "for sure".
      if (!in_array($field->getType(), self::ALLOWED_TYPES)
        || !str_starts_with($field_name, 'field_')
        || $paragraph->get($field_name)->isEmpty()
      ) {
        continue;
      }

      $parents = $top ? ['data', $field_name] : [$field_name];
      $value = $paragraph->get($field_name)->getValue();
      NestedArray::setValue($data, $parents, $value);
    }
  }

  /**
   * Generates a machine name from a string.
   *
   * This is basically the same as what is done in
   * \Drupal\Core\Block\BlockBase::getMachineNameSuggestion() and
   * \Drupal\system\MachineNameController::transliterate(), but it seems
   * that so far there is no common service for handling this.
   *
   * @param string $string
   *   The string to convert.
   *
   * @return string
   *   The generated machine name.
   *
   * @see \Drupal\Core\Block\BlockBase::getMachineNameSuggestion()
   * @see \Drupal\system\MachineNameController::transliterate()
   */
  protected function getMachineName($string): string {
    $transliterated = $this->transliteration->transliterate($string, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);

    $transliterated = preg_replace('@[^a-z0-9_.]+@', '_', $transliterated);

    $i = 0;
    $unique_suffix = '';
    while (ParagraphsSet::load($transliterated . $unique_suffix)) {
      // Append an incrementing numeric suffix until we find a unique name.
      $i++;
      $unique_suffix = '_' . $i;
    }

    return $transliterated . $unique_suffix;
  }

}
