<?php

declare(strict_types = 1);

namespace Drupal\Tests\paragraphs_sets_plugins_ui\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_sets_plugins_ui\ParagraphsSetsCreator;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

/**
 * Test basic field types.
 *
 * @group paragraphs_sets_plugins_ui
 */
final class BasicFieldTypesTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'file',
    'paragraphs',
    'paragraphs_sets_plugins_ui',
    'text',
    'link',
  ];

  /**
   * A default paragraph type for all tests.
   *
   * @var string
   */
  protected string $paragraphType = 'test_paragraph_type';

  /**
   * The paragraphs sets creator service.
   *
   * @var \Drupal\paragraphs_sets_plugins_ui\ParagraphsSetsCreator
   */
  protected ParagraphsSetsCreator $paragraphsSetsCreator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->addParagraphsType($this->paragraphType);

    $this->paragraphsSetsCreator = $this->container->get('paragraphs_sets_plugins_ui.creator');
  }

  /**
   * Test each field type provided by paragraphs_sets_plugins_ui.
   *
   * Check that every field type will provide a known structure when added to
   * a paragraph.
   *
   * @dataProvider dataParagraphWithFields
   */
  public function testParagraphWithFields($field_type, $field_data, $expected_structure) {

    $field_name = 'field_' . $field_type;

    // Add the field to the paragraph type.
    $this->addFieldtoParagraphType($this->paragraphType, $field_name, $field_type);

    // Create a node with a paragraph.
    $paragraph = Paragraph::create([
      'type' => $this->paragraphType,
      $field_name => $field_data,
    ]);

    // Let ParagraphsSetsCreator calculate the yml result.
    $structure = $this->paragraphsSetsCreator->prepareStructureTop([['entity' => $paragraph]], FALSE);

    // Assert the resulting structure.
    $expected = [
      [
        'type' => $this->paragraphType,
        'data' => [
          $field_name => $expected_structure,
        ],
      ],
    ];
    $this->assertEquals($expected, $structure);
  }

  /**
   * Provides data for testParagraphWithFields().
   *
   * @return array
   *   The test data array. The top level keys are free text but should be short
   *   and relate to the test case. The values are ordered arrays of test case
   *   data with elements that must appear in the following order:
   *   - Field type being tested.
   *   - Field value.
   *   - The expected result as accepted by paragraphs_sets module.
   */
  public function dataParagraphWithFields(): array {

    $data = [
      '1. Text (plain) (module: core)' => [
        // Field type.
        'string',
        // Field value.
        'Example text.',
        // Expected result.
        [
          [
            'value' => 'Example text.',
          ],
        ],
      ],
      '2. Text (formatted) (module: text)' => [
        // Field type.
        'text',
        // Field value.
        [
          'value' => '<p>Example text</p>',
          'format' => 'basic_html',
        ],
        // Expected result.
        [
          [
            'value' => '<p>Example text</p>',
            'format' => 'basic_html',
          ],
        ],
      ],
      '3. Link (module: link)' => [
        // Field type.
        'link',
        // Field value.
        [
          'uri' => 'https://example.com/',
          'title' => 'Example link.',
        ],
        // Expected result.
        [
          [
            'uri' => 'https://example.com/',
            'title' => 'Example link.',
            'options' => [],
          ],
        ],
      ],
    ];

    // Use unset $data['The key to remove']; to remove a temporarily unwanted
    // item, use return [$data['Key to test'], $data['Another']]; to selectively
    // test some items, or use return $data; to test everything.
    return $data;
  }

}
